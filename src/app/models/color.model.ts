import { Color } from './project.model';

export interface Project {
    name: string;
    description: string;
    link?: string;
    year: number;
    showYear?: boolean;
    color?: Color;
}
