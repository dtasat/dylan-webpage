export interface Quote {
    text: string;
    author: string;
    contact: string;
}
