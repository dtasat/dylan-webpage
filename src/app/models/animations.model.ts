
export interface Animations {
    titleFirstLineTranslation?: number;
    titleSecondLineTranslation?: number;
    titleThirdLineTranslation?: number;
    descriptionTranslation?: number;
    navigationItemsColor?: string;
    scrollButtonOpacity?: number;
    backgroundColor?: string;
    backgroundImageOpacity?: number;
    aboutTitleTranslation?: number;
    characteristicsTranslation?: number;
    quotesTranslations?: number[];
    quotesOpacities?: number[];
    projectsTitleTranslation?: number;
    timelineTranslation?: number;
    timelineOpacity?: number;
    projectsTranslation?: number;
    projectsOpacity?: number;
    contactTitleTranslation?: number;
    contactDescriptionTranslation?: number;
    contactButtonTranslation?: number;
    contactDataTranslation?: number;
    contactImageTranslation?: number;
    contactShadowOpening?: number;
}
