# Dylan's Webpage

Proyecto desarrollado en angular por Dylan Tasat.

## Instalación

Instalar Angular CLI (si no lo tienes):
- `npm install -g @angular/cli`

Instalar bibliotecas (solo al ejecutarlo por primera vez):
- `npm install`

## Servidor de desarrollo

Ejecutar con backend local:
- `ng serve`

Ejecutar con backend de producción:
- `ng serve --prod`

## Build

Build sin compresión:
- `ng build`

Build para producción:
- `ng build --prod --aot --vendor-chunk --common-chunk --delete-output-path --buildOptimizer`
